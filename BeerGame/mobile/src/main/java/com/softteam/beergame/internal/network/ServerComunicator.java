package com.softteam.beergame.internal.network;

import com.softteam.beergame.internal.constants.Constants;
import com.softteam.beergame.utils.EncryptUtils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map.Entry;

public class ServerComunicator {

    HttpClient httpclient = new DefaultHttpClient();
    HttpPost httppost = new HttpPost(Constants.SERVER_ADDRESS + "beergameweb");

    String httpGetAddress = Constants.SERVER_ADDRESS + "beergameweb?";
    HttpGet httpGet = new HttpGet(httpGetAddress);

    public void uploadScore(String nick, String country, int score) throws JSONException,
            ClientProtocolException, IOException {

        JSONObject jsonObject = new JSONObject();

        jsonObject.put("username", nick);
        jsonObject.put("score", score);
        jsonObject.put("area", country);

        httppost.setEntity(convertToHttpValues(jsonObject));

        // Execute HTTP Post Request
        HttpResponse response = httpclient.execute(httppost);

    }

    public ArrayList<Entry<String, String>> downloadRanking(String area) throws JSONException,
            ParseException, IOException {

        String jsonData = getDataFromUrl(area);
        ArrayList<Entry<String, String>> ranking = new ArrayList<Entry<String, String>>();
        JSONArray jScoreValues = new JSONArray();

        try {
            jScoreValues = new JSONArray(jsonData);

        } catch (Exception e) {
            return null;
        }

        for (int i = 0; i < jScoreValues.length(); i++) {

            JSONObject jScoreValue = (JSONObject) jScoreValues.get(i);

            Entry<String, String> entry = new com.softteam.beergame.internal.network.MyEntry<String, String>(jScoreValue.getString("username"),
                    jScoreValue.getString("score"));

            ranking.add(entry);

        }

        return ranking;

    }

    private UrlEncodedFormEntity convertToHttpValues(JSONObject jsonObject)
            throws UnsupportedEncodingException {

        String jData = jsonObject.toString();
        jData = encode(jData);

        ArrayList<NameValuePair> values = new ArrayList<NameValuePair>();

        values.add(new BasicNameValuePair("jsonData", jData));

        return new UrlEncodedFormEntity(values);

    }

    public String getDataFromUrl(String area) throws ParseException, IOException {
        InputStream content = null;

        HttpClient httpclient = new DefaultHttpClient();
        HttpResponse response = httpclient.execute(new HttpGet(httpGetAddress + "area=" + area));
        HttpEntity entity = response.getEntity();

        String jsonText = EntityUtils.toString(entity, HTTP.UTF_8);

        return decode(jsonText);

    }

    // ******************************** CRYPTOGRAPHY *****************
    private String encode(String text) {

        String cryptedJData = EncryptUtils.xorMessage(text, Constants.CRYPT_KEY);

        cryptedJData = com.softteam.beergame.utils.Base64
                .encodeToString(cryptedJData.getBytes(), false);

        return cryptedJData;
    }

    private String decode(String encryptedText) {

        String text = new String(com.softteam.beergame.utils.Base64.decode(encryptedText));

        text = EncryptUtils.xorMessage(text, Constants.CRYPT_KEY);

        return text;

    }

}
