package com.softteam.beergame.gameplay;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class BeerFactory {

	public static List<Beer> createBeers(String country,
			Map<String, String> beerMap) {

		List<Beer> beers = new ArrayList<Beer>();

		for (Entry<String, String> entry : beerMap.entrySet()) {

			Beer beer = new Beer();
			beer.setCountry(country);
			beer.setName(entry.getValue());
			beer.setImage(getAssetBeerPath(country, entry.getKey()));
			beer.setImageNoLabel(getAssetBeerNoLabelPath(country,
					entry.getKey()));

			beers.add(beer);

		}

		return beers;
	}

	private static String getAssetBeerPath(String country, String beerId) {

		return "beers/" + country + "/" + beerId + ".png";

	}

	private static String getAssetBeerNoLabelPath(String country, String beerId) {

		return "beers/" + country + "/" + beerId + "._.png";

	}
}
