package com.softteam.beergame.internal.interfaces;

import com.softteam.beergame.gameplay.BaseGameHandler;

public interface IScoreCounter {

	void startNewBeer();

	void registerGameHandler(BaseGameHandler baseGameHandler);

	void onAnswer(boolean isCorrectAnswer);

	int getMaxScoreForAnswer();

	int getTotalScore();

}
