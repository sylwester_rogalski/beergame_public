/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.softteam.beergame.BeerGameApplication;
import com.softteam.beergame.R;
import com.softteam.beergame.helpers.SwipeListener;
import com.softteam.beergame.internal.constants.Constants;
import com.softteam.beergame.preferences.PreferenceManager;
import com.softteam.beergame.preferences.Settings;

/**
 * @author Sylwester Rogalski
 * @class The SelectCountryActivity Class.
 * <p/>
 * @since 30 lis, 2014.
 */
public class SelectCountryActivity extends Activity implements View.OnClickListener {

    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_MAX_OFF_PATH = 250;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private GestureDetector gestureDetector;
    View.OnTouchListener gestureListener;

    private ImageSwitcher flagSwitcher;
    private ImageButton flagRightBtn;
    private ImageButton flagLeftBtn;
    private Button startBtn;

    private Animation inAnimationLeft;
    private Animation outAnimationLeft;
    private Animation inAnimationRight;
    private Animation outAnimationRight;
    private ViewPager mPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_country);

        // Gesture detection
        gestureDetector = new GestureDetector(this, new MyGestureDetector());
        gestureListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return gestureDetector.onTouchEvent(event);
            }
        };

        this.flagSwitcher = (ImageSwitcher) findViewById(R.id.flagSwitcher);
        this.flagLeftBtn = (ImageButton) findViewById(R.id.flagLeftBtn);
        this.flagRightBtn = (ImageButton) findViewById(R.id.flagRightBtn);
        this.startBtn = (Button) findViewById(R.id.startBtn);

        // flagswitcher configuration
        this.flagSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            @Override
            public View makeView() {
                ImageView imgView = new ImageView(SelectCountryActivity.this);
                imgView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                imgView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
//                imgView.getLayoutParams().width = (int) SelectCountryActivity.this.getResources().getDimension(R.dimen.select_country_flag_image_width);
//                imgView.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                return imgView;
            }
        });


        this.inAnimationLeft = AnimationUtils.loadAnimation(this, R.anim.push_in_left);
        this.outAnimationLeft = AnimationUtils.loadAnimation(this, R.anim.push_out_left);
        this.inAnimationRight = AnimationUtils.loadAnimation(this, R.anim.push_in_right);
        this.outAnimationRight = AnimationUtils.loadAnimation(this, R.anim.push_out_right);

        // flagleft/right configuration
        this.flagLeftBtn.setOnClickListener(this);
        this.flagRightBtn.setOnClickListener(this);
        this.startBtn.setOnClickListener(this);


        this.flagSwitcher.setOnTouchListener(new SwipeListener(BeerGameApplication.getContext()) {
            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                moveLeft();
            }

            @Override
            public void onSwipeRight() {
                super.onSwipeRight();
                moveRight();
            }
        });

        // set drawable
        setFlagDrawable();

    }

    @Override
    public void onClick(View clickedView) {

        if (clickedView.getId() == this.flagLeftBtn.getId()) {
            moveLeft();
        } else if (clickedView.getId() == this.flagRightBtn.getId()) {
            moveRight();
        } else if (clickedView.getId() == this.startBtn.getId()) {

            PreferenceManager.savePreference(Constants.SETTINGS_FLAG, Settings.currFlag);

            startActivity(new Intent(this, GameActivity.class));

        }

    }

    public void moveLeft() {
        this.flagSwitcher.setInAnimation(this.inAnimationLeft);
        this.flagSwitcher.setOutAnimation(this.outAnimationLeft);

        Settings.currFlag = Settings.getPrevFlag();

        setFlagDrawable();
    }

    public void moveRight() {
        this.flagSwitcher.setInAnimation(this.inAnimationRight);
        this.flagSwitcher.setOutAnimation(this.outAnimationRight);

        Settings.currFlag = Settings.getNextFlag();
        setFlagDrawable();
    }

    class MyGestureDetector extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                    return false;
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    moveRight();
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    moveLeft();
                }
            } catch (Exception e) {
                // Log.d
            }
            return false;
        }

    }

    private void setFlagDrawable() {

        String currFlag = Settings.currFlag;

        if (currFlag.equals(getApplicationContext().getString(R.string.country_all))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_all);
        } else if (currFlag.equals(getApplicationContext().getString(R.string.country_poland))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_poland);
        } else if (currFlag.equals(getApplicationContext().getString(R.string.country_spain))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_spain);
        } else if (currFlag.equals(getApplicationContext().getString(R.string.country_portugal))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_portugal);
        } else if (currFlag.equals(getApplicationContext().getString(R.string.country_germany))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_germany);
        } else if (currFlag.equals(getApplicationContext().getString(R.string.country_usa))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_usa);
        } else if (currFlag.equals(getApplicationContext().getString(R.string.country_chech))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_czech);
        } else if (currFlag.equals(getApplicationContext().getString(R.string.country_uk))) {
            this.flagSwitcher.setImageResource(R.drawable.flag_uk);
        }
    }
}
