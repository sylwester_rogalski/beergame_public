package com.softteam.beergame.internal.animations;

import android.graphics.drawable.ClipDrawable;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.Transformation;
import android.widget.ImageView;

public class PushDownAnimation extends Animation {

    private ImageView mView;
    private int mAnimationTime = 0;
    private int frames = 0;

    public PushDownAnimation(ImageView v, int animationTime, int frames) {

        this.mView = v;
        this.mAnimationTime = animationTime;
        this.frames = frames;

        setInterpolator(new LinearInterpolator());
        setDuration(animationTime);

    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        Log.d("GAME DEBUG", "applyTransformation: " + interpolatedTime + " frames: " + frames);

        //
        // if (interpolatedTime == 1.0) {
        // mView.getDrawable().setLevel(frames);
        // } else {
        ClipDrawable clipDrawable = (ClipDrawable) mView.getDrawable();
        clipDrawable.setLevel((int) (frames - (frames * interpolatedTime)));
        // }

        // mView.invalidate();

    }
}
