package com.softteam.beergame.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.softteam.beergame.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class RankingListAdapter extends ArrayAdapter<Entry<String, String>> {

    private List<Entry<String, String>> items;
    private Activity activity;

    public RankingListAdapter(Activity activity, int resource,
                              ArrayList<Entry<String, String>> scoreMap) {

        super(activity, resource, scoreMap);

        this.items = scoreMap;
        this.activity = activity;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.view_item_ranking_list, null);
            holder = new ViewHolder();
            holder.usernameTxt = (TextView) convertView.findViewById(R.id.usernameTxt);
            holder.scoreTxt = (TextView) convertView.findViewById(R.id.scoreTxt);
            holder.positionTxt = (TextView) convertView.findViewById(R.id.positionTxt);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Entry<String, String> custom = items.get(position);

        if (custom != null) {
            holder.positionTxt.setText(String.valueOf(position + 1) + ".");
            holder.usernameTxt.setText(custom.getKey());
            holder.scoreTxt.setText(custom.getValue());
        }

        return convertView;

    }

    public static class ViewHolder {
        public TextView positionTxt;
        public TextView usernameTxt;
        public TextView scoreTxt;
    }
}