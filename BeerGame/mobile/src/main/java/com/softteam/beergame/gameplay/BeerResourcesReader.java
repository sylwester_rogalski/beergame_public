package com.softteam.beergame.gameplay;

import android.content.res.XmlResourceParser;
import android.util.Log;

import com.softteam.beergame.BeerGameApplication;
import com.softteam.beergame.R;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BeerResourcesReader {

    public Map<String, String> readBeersForCountry(String country) {

        XmlResourceParser xml = null;

        try {
            Map<String, String> beers = new HashMap<String, String>();

            xml = BeerGameApplication.getContext().getResources().getXml(R.xml.beers);

            int element = XmlResourceParser.START_DOCUMENT;

            try {

                do {

                    element = xml.next();

                    if (element == XmlResourceParser.START_TAG && xml.getName().equals("country")) {

                        if (xml.getAttributeValue(null, "name").equals(country)) {

                            element = xml.nextTag();

                            do {

                                String beerId = xml.getAttributeValue(null, "id");

                                String beerName = xml.nextText();

                                beers.put(beerId, beerName);

                                Log.e(beerId, beerName);

                                element = xml.nextTag();

                            }
                            while (element == XmlResourceParser.START_TAG && xml.getName().equals("beer"));

                        }

                    }

                } while (element != XmlResourceParser.END_DOCUMENT);

            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return beers;
        } finally {
            if (xml != null)
                xml.close();
        }
    }

    // public Map<String, Map<String, String>> readBeersForCountry() {
    //
    // XmlResourceParser xml = null;
    //
    // try {
    // Map<String, Map<String, String>> beers = new HashMap<String, Map<String,
    // String>>();
    //
    // xml = ApplicationBeer.getInstance().getResources().getXml(R.xml.beers);
    //
    // int element = XmlResourceParser.START_DOCUMENT;
    //
    // try {
    //
    // do {
    //
    // element = xml.next();
    //
    // if (element == XmlResourceParser.START_TAG &&
    // xml.getName().equals("country")) {
    //
    // String country = xml.getAttributeValue(null, "name");
    //
    // do {
    //
    // element = xml.nextTag();
    //
    // String beerId = xml.getAttributeValue(null, "id");
    //
    // String beerName = xml.nextText();
    //
    // if (!beers.containsKey(country)) {
    // beers.put(country, new HashMap<String, String>());
    // }
    //
    // beers.get(country).put(beerId, beerName);
    //
    // Log.e(beerId, beerName);
    //
    // if (element == XmlResourceParser.END_TAG &&
    // xml.getName().equals("country")) {
    // break;
    // }
    //
    // } while (element == XmlResourceParser.START_TAG &&
    // xml.getName().equals("beer"));
    //
    // }
    //
    // } while (element != XmlResourceParser.END_DOCUMENT);
    //
    // } catch (XmlPullParserException e) {
    // e.printStackTrace();
    // } catch (IOException e) {
    // e.printStackTrace();
    // }
    //
    // return beers;
    // } finally {
    // if (xml != null)
    // xml.close();
    // }
    // }

}
