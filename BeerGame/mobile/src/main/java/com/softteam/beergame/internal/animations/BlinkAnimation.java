package com.softteam.beergame.internal.animations;

import android.util.FloatMath;
import android.view.animation.Animation;
import android.view.animation.Interpolator;
import android.view.animation.Transformation;

public class BlinkAnimation extends Animation {
	private int totalBlinks;
	private boolean finishOff;

	public BlinkAnimation(int animationTime, Interpolator interpolator,
			int totalBlinks, boolean finishOff) {

		this.totalBlinks = totalBlinks;
		this.finishOff = finishOff;

		setInterpolator(interpolator);
		setDuration(animationTime);

	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		float period = interpolatedTime * totalBlinks * 3.14f
				+ (finishOff ? 3.14f / 2 : 0);
		t.setAlpha(Math.abs(FloatMath.cos(period)));
	}
}