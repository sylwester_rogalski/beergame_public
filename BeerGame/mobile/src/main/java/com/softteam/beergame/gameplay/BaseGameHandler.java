package com.softteam.beergame.gameplay;

import android.os.Handler;
import android.util.Log;

import com.softteam.beergame.internal.constants.Constants;
import com.softteam.beergame.internal.interfaces.IGameUiListener;
import com.softteam.beergame.internal.interfaces.IScoreCounter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;

public abstract class BaseGameHandler {

    private IGameUiListener gameUiListener;
    private IScoreCounter scoreCounter;

    private int beerIndex = -1;
    private List<Beer> beers;

    private Random random;

    private int answersNumber = 3;

    public BaseGameHandler(List<Beer> beers) {
        this.beers = beers;
        this.random = new Random(new Date().getTime());
        this.scoreCounter = new DefaultScoreCounter(this);
    }

    public void registerUiListener(IGameUiListener gameUiListener) {

        this.gameUiListener = gameUiListener;
    }

    private void publishBeer() {

        gameUiListener.onNextBeer(getCurrentBeer());
        gameUiListener.onUpdateAnswers(randomizeAnswers(this.answersNumber));

        scoreCounter.startNewBeer();
    }

    //

    public void uiBeginGame() {

        gameUiListener.onPlay(this.beers.size());
        nextBeer();
    }

    public void uiExitGame() {

        Log.e("ERROR", "No beers passed");
        gameUiListener.onExit();

    }

    private String[] randomizeAnswers(int answersNumber) {

        List<String> selectedAnswers = new ArrayList<String>();

        List<Beer> randomizedBeers = randomBeers(answersNumber);
        for (Beer beer : randomizedBeers) {
            selectedAnswers.add(beer.getName());
        }

        return selectedAnswers.toArray(new String[0]);
    }

    //
    public void selectAnswer(String answer) {

        boolean isCorrectAnswer = answer.equals(getCurrentBeer().getName());

        gameUiListener.onAnswer(isCorrectAnswer);
        scoreCounter.onAnswer(isCorrectAnswer);

        Handler handler = new Handler();
        Runnable task = new Runnable() {

            public void run() {
                nextBeer();
            }
        };

        handler.postDelayed(task, Constants.WAIT_BEFORE_NEXT_BEER);

    }

    private List<Beer> randomBeers(int count) {

        if (count > beers.size())
            count = beers.size();

        List<Beer> selectedBeers = new ArrayList<Beer>();
        List<Beer> beersClone = new ArrayList<Beer>(beers);

        selectedBeers.add(beersClone.remove(this.beerIndex));

        for (int i = 0; i < count - 1; i++) {

            if (i < selectedBeers.size()) {

                int randomizedIndex = this.random.nextInt(beersClone.size());
                Beer selectedBeer = beersClone.remove(randomizedIndex);

                selectedBeers.add(selectedBeer);

            }

        }

        Collections.shuffle(selectedBeers);

        return selectedBeers;
    }

    private boolean hasNextBeer() {

        return (beers.size() - 1 > beerIndex);
    }

    private void nextBeer() {

        if (hasNextBeer()) {
            beers.get(++beerIndex);

            publishBeer();
        } else {
            gameUiListener.onEndGame(scoreCounter.getTotalScore());
        }

    }

    private Beer getCurrentBeer() {
        return beers.get(beerIndex);
    }

    public void setNewScore(int score) {

        gameUiListener.onScoreChanged(score);
    }

    public int getAnimationTime() {
        return scoreCounter.getMaxScoreForAnswer();
    }

}
