/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame.preferences;

import android.app.Activity;
import android.content.SharedPreferences;

import com.softteam.beergame.BeerGameApplication;

/**
 * @author Sylwester Rogalski
 * @class The PreferenceManager Class.
 * <p/>
 * @since 01 gru, 2014.
 */
public class PreferenceManager {

    public static void savePreference(String key, String value) {

        SharedPreferences settings = BeerGameApplication.getContext().getSharedPreferences("settings", Activity.MODE_PRIVATE);
        SharedPreferences.Editor settingsEditor = settings.edit();
        settingsEditor.putString(key, value);
        settingsEditor.commit();

    }

    public static String readPreference(String key) {

        SharedPreferences settings = BeerGameApplication.getContext().getSharedPreferences("settings", Activity.MODE_PRIVATE);

        String value = settings.getString(key, null);

        return value;
    }
}
