package com.softteam.beergame.internal.interfaces;


import com.softteam.beergame.gameplay.Beer;

public interface IGameUiListener {

    public void onPlay(int beersQuantity);

    public void onNextBeer(Beer beer);

    public void onTimeOver();

    public void onEndGame(int score);

    public void onExit();

    public void onTick();

    public void onAnswer(boolean isCorrectAnswer);

    public void onUpdateAnswers(String[] answers);

    public void onScoreChanged(int score);
}
