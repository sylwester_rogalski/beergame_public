/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame.gameplay;

import com.softteam.beergame.BeerGameApplication;
import com.softteam.beergame.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Sylwester Rogalski
 * @class The CountryBeer Class.
 * <p/>
 * @since 02 gru, 2014.
 */
public class CountryBeer {

    private static BeerResourcesReader beerResourcesReader = new BeerResourcesReader();

    public static List<Beer> getBeers(String country) {

        List<Beer> result = new ArrayList<Beer>();

        if (country.equals(BeerGameApplication.getContext().getString(R.string.country_all))) {

            String[] flags = BeerGameApplication.getContext().getResources().getStringArray(R.array.countries);

            for (String flag : flags) {

                result.addAll(BeerFactory.createBeers(flag,
                        beerResourcesReader.readBeersForCountry(flag)));
            }

        } else {
            result = BeerFactory.createBeers(country, beerResourcesReader.readBeersForCountry(country));
        }

        Collections.shuffle(result);// shufle

        int end = result.size() > 15 ? 15 : result.size();

        return result.subList(0, end);
    }
}
