package com.softteam.beergame.utils;

public class EncryptUtils {

	/*
	 * // public static final String DEFAULT_ENCODING = "UTF-8"; // // public
	 * static String base64encode(String text) { // // String rez =
	 * Base64.encodeBase64String(text.getBytes()); // return rez; // // }//
	 * base64encode // // public static String base64decode(String text) { // //
	 * base64 = new Base64InputStream(new StringReader(text), flags) // //
	 * String rez = new String(decodeBase64(text)); // return rez; // // }//
	 * base64decode
	 * 
	 * // public static void main(String[] args) { // String txt =
	 * "some text to be encrypted"; // String key =
	 * "key phrase used for XOR-ing"; // System.out.println(txt + " XOR-ed to: "
	 * + (txt = xorMessage(txt, key))); // String encoded = base64encode(txt);
	 * // System.out.println(" is encoded to: " + encoded // +
	 * " and that is decoding to: " + (txt = base64decode(encoded))); //
	 * System.out.print("XOR-ing back to original: " + xorMessage(txt, key)); //
	 * // }
	 */
	public static String xorMessage(String message, String key) {
		try {
			if (message == null || key == null)
				return null;

			char[] keys = key.toCharArray();
			char[] mesg = message.toCharArray();

			int ml = mesg.length;
			int kl = keys.length;
			char[] newmsg = new char[ml];

			for (int i = 0; i < ml; i++) {
				newmsg[i] = (char) (mesg[i] ^ keys[i % kl]);
			}// for i
			mesg = null;
			keys = null;
			return new String(newmsg);
		} catch (Exception e) {
			return null;
		}
	}// xorMessage

}// class