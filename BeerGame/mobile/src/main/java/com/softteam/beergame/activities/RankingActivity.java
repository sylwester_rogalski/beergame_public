/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.softteam.beergame.R;
import com.softteam.beergame.adapters.RankingListAdapter;
import com.softteam.beergame.internal.network.ServerComunicator;
import com.softteam.utilslibrary.logs.Logger;

import java.util.ArrayList;
import java.util.Map;

/**
 * @author Sylwester Rogalski
 * @class The RankingActivity Class.
 * <p/>
 * @since 30 lis, 2014.
 */
public class RankingActivity extends Activity {

    public static ArrayList<Map.Entry<String, String>> scoreMap;

    private ListView mRankingListView;
    public ProgressDialog mProgressDialog;
    private Spinner mCountrySpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ranking);

        mRankingListView = (ListView) findViewById(R.id.rankingListView);
        mCountrySpinner = (Spinner) findViewById(R.id.countrySpinner);

        Bundle bundle = getIntent().getExtras();

        String area = getApplicationContext().getString(R.string.country_all);
        if (bundle != null && bundle.containsKey(getApplicationContext().getString(R.string.ranking_extra_key_area))) {
            area = bundle.getString(getApplicationContext().getString(R.string.ranking_extra_key_area));
        }

        setupSpinner(area);
    }

    private void setupSpinner(String area) {

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.view_spinner, R.id.spinner_item);

        Integer selectedPosition = null;
        for (String country : getApplicationContext().getResources().getStringArray(R.array.countries)) {

            adapter.add(country);

            if (country.equalsIgnoreCase(area)) {
                selectedPosition = adapter.getCount() - 1;
            }

        }

        this.mCountrySpinner.setAdapter(adapter);
        this.mCountrySpinner.setSelection(selectedPosition);

        this.mCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView selectedView = (TextView) view.findViewById(R.id.spinner_item);
                new RankingDownloader(selectedView.getText().toString()).execute();
            }

            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

    }

    class RankingDownloader extends AsyncTask<Void, Void, Void> {

        // private LinkedHashMap<String, String> item_ranking;
        private boolean isRankingDownloaded = false;
        private String area;

        public RankingDownloader(String area) {
            this.area = area;
        }

        @Override
        protected void onPreExecute() {

            mProgressDialog = new ProgressDialog(RankingActivity.this);
            mProgressDialog.setMessage("Downloading current item_ranking..");
            mProgressDialog.setCancelable(true);

            mProgressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {

                public void onCancel(DialogInterface dialog) {
                    Intent intent = new Intent(RankingActivity.this, MainMenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            });

            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                scoreMap = new ServerComunicator().downloadRanking(this.area);
                isRankingDownloaded = true;
            } catch (Exception e) {
                Logger.e(getClass(), e);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgressDialog.dismiss();

            if (isRankingDownloaded) {

                RankingListAdapter listAdapter = new RankingListAdapter(RankingActivity.this,
                        R.id.rankingListView, scoreMap);
                RankingActivity.this.mRankingListView.setAdapter(listAdapter);

            } else {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(RankingActivity.this);
                dialogBuilder.setMessage(getApplicationContext().getString(R.string.ranking_check_internet_connection));

                dialogBuilder.setNegativeButton(getApplicationContext().getString(R.string.ranking_go_mainmenu), new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Intent intent = new Intent(RankingActivity.this, MainMenuActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                    }
                });

                dialogBuilder.setPositiveButton(getApplicationContext().getString(R.string.general_retry), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        new RankingDownloader(RankingDownloader.this.area).execute();
                    }
                });
                dialogBuilder.show();
            }

        }
    }
}
