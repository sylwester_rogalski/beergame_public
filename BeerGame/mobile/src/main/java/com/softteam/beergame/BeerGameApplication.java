/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame;

import android.app.Application;
import android.content.Context;

/**
 * @author Sylwester Rogalski
 * @class The BeerGameApplication Class.
 * <p/>
 * @since 30 lis, 2014.
 */
public class BeerGameApplication extends Application {

    private static Context sContext;


    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
    }


    public static Context getContext() {
        return sContext;
    }
}
