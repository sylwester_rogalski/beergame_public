/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame.preferences;

import com.softteam.beergame.BeerGameApplication;
import com.softteam.beergame.R;
import com.softteam.beergame.internal.constants.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Sylwester Rogalski
 * @class The Settings Class.
 * <p/>
 * @since 01 gru, 2014.
 */
public class Settings {

    public static String currFlag;

    private static List<String> sFlags;
//    private BeerResourcesReader beerResourcesReader = new BeerResourcesReader();


    static {
        sFlags = new ArrayList<String>();
        sFlags.addAll(Arrays.asList(BeerGameApplication.getContext().getResources().getStringArray(R.array.countries)));

        String flagName;

        flagName = PreferenceManager.readPreference(Constants.SETTINGS_FLAG);

        try {
            int value = Integer.parseInt(flagName);
            flagName = BeerGameApplication.getContext().getString(R.string.country_all);
        } catch (NumberFormatException nfe) {
        }

        if (flagName == null) {
            flagName = BeerGameApplication.getContext().getString(R.string.country_all);
        }

        currFlag = flagName;
    }

    public static String getNextFlag() {
        for (int i = 0; i < sFlags.size(); i++) {
            String flag = sFlags.get(i);
            if (flag.equals(currFlag)) {
                if (i == sFlags.size() - 1) {
                    return sFlags.get(0);
                } else {
                    return sFlags.get(i + 1);
                }
            }
        }

        return currFlag;
    }

    public static String getPrevFlag() {
        for (int i = 0; i < sFlags.size(); i++) {
            String flag = sFlags.get(i);
            if (flag.equals(currFlag)) {
                if (i == 0) {
                    return sFlags.get(sFlags.size() - 1);
                } else {
                    return sFlags.get(i - 1);
                }
            }
        }

        return currFlag;
    }

}
