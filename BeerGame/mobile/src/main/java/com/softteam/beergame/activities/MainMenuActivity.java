package com.softteam.beergame.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.android.apps.analytics.GoogleAnalyticsTracker;
import com.softteam.beergame.R;


public class MainMenuActivity extends Activity implements View.OnClickListener {

    private Button mPlayBtn;
    private Button mRankingBtn;
    private GoogleAnalyticsTracker mTracker;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_menu);

        this.mPlayBtn = (Button) findViewById(R.id.main_menu_button_play);
        this.mRankingBtn = (Button) findViewById(R.id.main_menu_button_ranking);

        this.mRankingBtn.setOnClickListener(this);
        this.mPlayBtn.setOnClickListener(this);

        this.mTracker = GoogleAnalyticsTracker.getInstance();
        // Start the mTracker in manual dispatch mode...
        this.mTracker.startNewSession("UA-34781092-1", 20, this);

    }

    // event listeners
    public void onClick(View clickedViev) {

        if (clickedViev.getId() == this.mPlayBtn.getId()) {
            mTracker.trackPageView("/mPlayBtn");
            mTracker.dispatch();

            Intent intent = new Intent(this, SelectCountryActivity.class);
            startActivity(intent);
        } else if (clickedViev.getId() == mRankingBtn.getId()) {

            Intent intent = new Intent(this, RankingActivity.class);
            startActivity(intent);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTracker.stopSession();
    }

}
