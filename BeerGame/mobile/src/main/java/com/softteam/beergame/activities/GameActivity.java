/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame.activities;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.google.ads.AdRequest;
import com.google.ads.AdSize;
import com.google.ads.AdView;
import com.softteam.beergame.R;
import com.softteam.beergame.gameplay.BaseGameHandler;
import com.softteam.beergame.gameplay.Beer;
import com.softteam.beergame.gameplay.CountryBeer;
import com.softteam.beergame.gameplay.DefaultGameHandler;
import com.softteam.beergame.internal.animations.PushDownAnimation;
import com.softteam.beergame.internal.interfaces.IGameUiListener;
import com.softteam.beergame.preferences.Settings;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Sylwester Rogalski
 * @class The GameActivity Class.
 * <p/>
 * @since 30 lis, 2014.
 */
public class GameActivity extends Activity implements View.OnClickListener, IGameUiListener {

    ImageView bearImg;
    //    ImageView mugImg;
    ImageSwitcher mButtleImg;
    ClipDrawable back;

    private AdView adView;

    Beer beer;
    // BeerChanger beerChanger = new BeerChanger();
    BaseGameHandler gameHandler;

    final int DRAWABLE_FRAMES = 10000;

    int beersQuantity = 0;
    int beerNumber = 0;

    private Button answerBtn1;
    private Button answerBtn2;
    private Button answerBtn3;

    private TextView scoreValueTxt;

    private List<Button> buttonAnswers = new ArrayList<Button>();
    private Animation animationInButtle;
    private Animation animationOutButtle;

    private Animation animationInButtleLabel;
    private Button clickedButton;

    private TextView beerCounterTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game);

        // bearImg = (ImageView) findViewById(R.id.piwoImg);

        this.answerBtn1 = (Button) findViewById(R.id.answerBtn1);
        this.answerBtn1.setOnClickListener(this);

        this.answerBtn2 = (Button) findViewById(R.id.answerBtn2);
        this.answerBtn2.setOnClickListener(this);

        this.answerBtn3 = (Button) findViewById(R.id.answerBtn3);
        this.answerBtn3.setOnClickListener(this);

        this.beerCounterTxt = (TextView) findViewById(R.id.beerCounterTxt);

        buttonAnswers.add(answerBtn1);
        buttonAnswers.add(answerBtn2);
        buttonAnswers.add(answerBtn3);

        bearImg = (ImageView) findViewById(R.id.piwoImg);
        bearImg.getDrawable().setLevel(10000);

//        mugImg = (ImageView) findViewById(R.id.kufelImg);

        // buttleImg
        mButtleImg = (ImageSwitcher) findViewById(R.id.buttle);

        this.mButtleImg.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                ImageView imgView = new ImageView(GameActivity.this);

                imgView.setScaleType(ImageView.ScaleType.CENTER);
                imgView.setAdjustViewBounds(true);
                return imgView;
            }
        });

        this.animationInButtle = AnimationUtils.loadAnimation(this, R.anim.buttle_in);
        this.animationOutButtle = AnimationUtils.loadAnimation(this, R.anim.buttle_out);

        // gamehandler
        gameHandler = new DefaultGameHandler(CountryBeer.getBeers(Settings.currFlag));
        gameHandler.registerUiListener(this);

        this.scoreValueTxt = (TextView) findViewById(R.id.scoreValue);

        addAdView();
        // beerChanger.nextBeer();
    }

    private void addAdView() {

        // Create the adView
        adView = new AdView(this, AdSize.SMART_BANNER, "a1505615c789eb5");

        // Lookup your LinearLayout assuming it's been given
        // the attribute android:id="@+id/mainLayout"
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.adBanner);

        // Add the adView to it

        layout.addView(adView);

        // ///08-18 02:11:45.710: I/Ads(2801): To get test ads on this device,
        // call adRequest.addTestDevice("BCED9018DC271299504BE5AF95EBABA6");
        //
        AdRequest adRequest = new AdRequest();
        // adRequest.addTestDevice(AdRequest.TEST_EMULATOR); // Emulator
        // adRequest.addTestDevice("7B936411CA85B3A6FE5E5EA942233609"); // Test
        // Android
        // Device

        // //

        // Initiate a generic request to load it with an ad
        adView.loadAd(adRequest);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        gameHandler.uiBeginGame();
    }

    public void onClick(View clickedView) {

        Integer clickedViewId = clickedView.getId();

        if (clickedView instanceof Button) {

            this.clickedButton = (Button) clickedView;

            if (clickedViewId == R.id.answerBtn1 || clickedViewId == R.id.answerBtn2
                    || clickedViewId == R.id.answerBtn3) {

                gameHandler.selectAnswer((String) clickedButton.getText());

            }
        }
        // else if(clic) TODO if clicked bonus

    }

    public void onPlay(int beersNumber) {

        this.beersQuantity = beersNumber;
        // log("onPlay");

    }

    public void onNextBeer(Beer beer) {
        // log("onNextBeer");

        setBeer(beer, false);

    }

    public void onTimeOver() {
        // log("onTimeOver");
    }

    public void onEndGame(int score) {

        Intent intent = new Intent(this, AchivedScoreActivity.class);
        intent.putExtra("score", score);
        startActivity(intent);

        // log("onEndGame");
    }

    public void onTick() {
        // log("onTick");
    }

    private void disableAnswerButtons() {

        for (int i = 0; i < buttonAnswers.size(); i++) {

            Button currButton = buttonAnswers.get(i);

            currButton.setEnabled(false);

        }

    }

    public void onAnswer(boolean isCorrectAnswer) {
        // log("onGoodAnswer");

        disableAnswerButtons();

        if (isCorrectAnswer == false) {
            if (clickedButton != null) {
                clickedButton.setBackgroundResource(R.drawable.item_button_wrong);
                clickedButton.setTextColor(Color.WHITE);
            }
        } else {
            if (clickedButton != null) {
                clickedButton.setBackgroundResource(R.drawable.item_button_correct);
                clickedButton.setTextColor(Color.WHITE);
            }
        }

        setBeer(this.beer, true);

        revertAnimation();
    }

    public void onUpdateAnswers(String[] answers) {

        for (int i = 0; i < answers.length; i++) {

            if (buttonAnswers.size() > i) {

                Button currButton = buttonAnswers.get(i);

                currButton.setEnabled(true);
                currButton.setText(answers[i]);
                currButton.setTextColor(getResources().getColor(R.color.text_color));
                currButton.setBackgroundResource(R.drawable.button_drawable);

            }

        }

    }

    public void onExit() {

        goMainMenu();

    }

    private void goMainMenu() {
        Intent intent = new Intent(GameActivity.this, MainMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void setBeer(Beer beer, boolean withLabel) {

        this.beer = beer;

        // on answer
        if (withLabel) {

            mButtleImg.setInAnimation(null);
            mButtleImg.setOutAnimation(animationOutButtle);

            setBeerImage(beer.getImage());

            mButtleImg.getChildAt(0).bringToFront();

        } else {// on new beer

            this.beerNumber++;
            this.beerCounterTxt.setText(this.beerNumber + "/" + this.beersQuantity);

            mButtleImg.setInAnimation(animationInButtle);
            mButtleImg.setOutAnimation(null);

            setBeerImage(beer.getImageNoLabel());

            startAnimation(gameHandler.getAnimationTime());

        }
    }

    private void setBeerImage(String imagePath) {

        Drawable buttleDrawable = null;
        InputStream in = null;

        try {
            in = getAssets().open(imagePath);

            buttleDrawable = BitmapDrawable.createFromStream(in, "assets");

            mButtleImg.setImageDrawable(buttleDrawable);

            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void log(String text) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
        Log.d("GAME DEBUG", text);
    }

    public void onScoreChanged(int score) {
        this.scoreValueTxt.setText(String.valueOf(score));
    }

    private void startAnimation(int animationTime) {
        Log.d("GAME DEBUG", "Start Animation Beer Mug");

        Animation drinkBearAnimation = new PushDownAnimation(bearImg, animationTime, DRAWABLE_FRAMES);
        bearImg.startAnimation(drinkBearAnimation);
    }

    public void revertAnimation() {
        bearImg.getDrawable().setLevel(0);
    }

    @Override
    public void onBackPressed() {

        goMainMenu();

    }


}
