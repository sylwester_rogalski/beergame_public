package com.softteam.beergame.gameplay;

import com.softteam.beergame.internal.interfaces.IScoreCounter;
import com.softteam.beergame.utils.StopWatch;

import java.util.Timer;

public class DefaultScoreCounter implements IScoreCounter {

	BaseGameHandler gameHandler;

	Timer answerTimer;

	int totalScore = 0;

	int maxTimeToAnswer = 10000;

	StopWatch stopWatch;

	public DefaultScoreCounter(BaseGameHandler gameHandler) {
		registerGameHandler(gameHandler);
	}

	public void startNewBeer() {

		this.stopWatch = new StopWatch();
		this.stopWatch.start();

	}

	public void onAnswer(boolean isCorrectAnswer) {

		this.stopWatch.stop();
		long elapsedTime = this.stopWatch.getElapsedTime();

		if (elapsedTime > maxTimeToAnswer) {
			elapsedTime = maxTimeToAnswer;
		}

		int score = 0;
		if (isCorrectAnswer) {
			score = (int) (maxTimeToAnswer - elapsedTime);
		} else {
			score = (int) (elapsedTime - maxTimeToAnswer);
		}

		addScore(score);
		setNewScore();
	}

	private void addScore(int score) {
		this.totalScore += score;

		if (this.totalScore < 0) {
			this.totalScore = 0;
		}
	}

	public void registerGameHandler(BaseGameHandler gameHandler) {

		this.gameHandler = gameHandler;

	}

	private void timeToAnswerOver() {

	}

	private void setNewScore() {
		gameHandler.setNewScore(this.totalScore);
	}

	public int getTotalScore() {
		return this.totalScore;
	}

	public int getMaxScoreForAnswer() {
		return maxTimeToAnswer;
	}

}
