package com.softteam.beergame.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.softteam.beergame.R;
import com.softteam.beergame.internal.constants.Constants;
import com.softteam.beergame.internal.network.ServerComunicator;
import com.softteam.beergame.preferences.PreferenceManager;
import com.softteam.beergame.preferences.Settings;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import java.io.IOException;
import java.util.Date;
import java.util.Random;

public class AchivedScoreActivity extends Activity implements OnClickListener {

    private Integer achivedScore;

    private TextView achivedScoreTxt;
    private Button sendScoreBtn;
    private Button seeRankingBtn;
    private Button okBtn;
    private TextView categoryTxt;

    private ProgressDialog currDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achived_score);

        this.achivedScore = getIntent().getExtras().getInt("score", -1);

        // find views by id
        this.achivedScoreTxt = (TextView) findViewById(R.id.achivedScoreTxt);
        this.sendScoreBtn = (Button) findViewById(R.id.sendScoreBtn);
        this.seeRankingBtn = (Button) findViewById(R.id.seeRankingBtn);
        this.okBtn = (Button) findViewById(R.id.okBtn);
        this.categoryTxt = (TextView) findViewById(R.id.categoryTxt);

        // set text on views
        this.achivedScoreTxt.setText(String.valueOf(achivedScore));

        // add action listeners
        this.seeRankingBtn.setOnClickListener(this);
        this.sendScoreBtn.setOnClickListener(this);
        this.okBtn.setOnClickListener(this);

        //
        this.categoryTxt.setText(Settings.currFlag);

    }

    @Override
    public void onClick(View view) {

        int viewId = view.getId();

        if (viewId == sendScoreBtn.getId()) {

            String currUsername = PreferenceManager.readPreference(Constants.SETTINGS_USERNAME);

            if (currUsername == null) {
                currUsername = "Happy Anonim";
            }

            createInputDialog(currUsername);

        } else if (viewId == seeRankingBtn.getId()) {

            Intent intent = new Intent(this, RankingActivity.class);
            intent.putExtra("area", Settings.currFlag);
            startActivity(intent);
        } else if (viewId == okBtn.getId()) {

            goMainMenu();

        }

    }

    private void goMainMenu() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    //
    @Override
    public void onBackPressed() {

        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);

    }

    class ScoreSenderTask extends AsyncTask<Void, Void, Void> {

        boolean isScoreSent = false;
        private final String country;
        String username = null;

        public ScoreSenderTask(String currUsername, String categoryCountry) {
            this.username = currUsername;
            this.country = categoryCountry;
        }

        @Override
        protected void onPreExecute() {

            currDialog = new ProgressDialog(AchivedScoreActivity.this);
            currDialog.setMessage("Score sending...");
            currDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            try {
                Random rand = new Random(new Date().getTime());
                new ServerComunicator().uploadScore(this.username, this.country, achivedScore);

                this.isScoreSent = true;

            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {

            currDialog.dismiss();

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(AchivedScoreActivity.this);
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            if (this.isScoreSent) {
                dialogBuilder.setMessage("Your score has been sent");
                AchivedScoreActivity.this.sendScoreBtn.setEnabled(false);
            } else {
                dialogBuilder.setMessage("Score can not be send.\nCheck network connection.");
            }

            dialogBuilder.show();
        }
    }

    private void createInputDialog(String currUsername) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        alert.setTitle("What is your username?");
        alert.setMessage("Type Your nick: ");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setText(currUsername);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String currUsername = input.getText().toString();

                PreferenceManager.savePreference(Constants.SETTINGS_USERNAME, currUsername);
                new ScoreSenderTask(currUsername, Settings.currFlag).execute();
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                // Canceled.
            }
        });

        alert.show();
    }

}
