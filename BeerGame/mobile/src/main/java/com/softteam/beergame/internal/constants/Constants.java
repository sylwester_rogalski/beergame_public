/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.beergame.internal.constants;

/**
 * @author Sylwester Rogalski
 * @class The ConstantsNetwork Class.
 * <p/>
 * @since 30 lis, 2014.
 */
public class Constants {

    public static final String SERVER_ADDRESS = "http://beargame-app.appspot.com/";
    public static final String CRYPT_KEY = "BEER GAME IT IS THE BEST GAME EVER";

    public static final String SETTINGS_USERNAME = "username";
    public static final String SETTINGS_FLAG = "flag";
    public static final int WAIT_BEFORE_NEXT_BEER = 1000;
}
