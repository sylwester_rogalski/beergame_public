package com.softteam.beergame.gameplay;


public class Beer {

	String country;
	String name;
	String image;
	String imageNoLabel;

	public Beer() {

	}

	public Beer(String country, String name, String image, String imageNoLabel) {
		super();
		this.country = country;
		this.name = name;
		this.image = image;
		this.imageNoLabel = imageNoLabel;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getImageNoLabel() {
		return imageNoLabel;
	}

	public void setImageNoLabel(String imageNoLabel) {
		this.imageNoLabel = imageNoLabel;
	}

}
