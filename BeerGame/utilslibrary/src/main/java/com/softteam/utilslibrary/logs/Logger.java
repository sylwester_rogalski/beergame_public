/*
 * Copyright (C) 2012 The Andrino Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.softteam.utilslibrary.logs;

import android.util.Log;

/**
 * @author Sylwester Rogalski
 * @class The Logger Class.
 * <p/>
 * @since 30 lis, 2014.
 */
public class Logger {

    public static void d(String message) {
        d("Logger", message);
    }

    public static void d(String tag, String message) {
        Log.d(tag, message);
    }

    public static void d(Class type, String message) {
        d(type.getName(), message);
    }

    public static void e(Class type, Exception e) {
        e(type.getName(), e);
    }

    public static void e(String tag, Exception e) {
        e(tag, Log.getStackTraceString(e));
    }

    public static void e(String tag, String message) {
        Log.e(tag, message);
    }
}
